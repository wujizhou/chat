package com.rainbow.chat.jmx;

import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.management.JMX;
import javax.management.MBeanServerConnection;
import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;
import java.io.IOException;

@SpringBootTest
public class JmxTest {
    private static final String URL = ":9008";
    private static final String PACKAGE = "com.zhongwang.chat.jmx:type=";
    private ObjectName getObjectName(String name) throws MalformedObjectNameException {
        return new ObjectName(name);
    }

    private JMXConnector getJmxConnect() throws IOException {
        // *** VM options ***
        // -Dcom.sun.management.jmxremote.port=9008
        // -Dcom.sun.management.jmxremote.authenticate=false
        // -Dcom.sun.management.jmxremote.ssl=false
        JMXServiceURL url = new JMXServiceURL("service:jmx:rmi:///jndi/rmi://"+ URL+"/jmxrmi");
        return JMXConnectorFactory.connect(url, null);
    }


    @Test
    public void test() throws IOException, MalformedObjectNameException {
        MBeanServerConnection mbsc = getJmxConnect().getMBeanServerConnection();
        // Create a dedicated proxy for the MBean instead of going directly through the MBean server connection
        IChatM mbeanProxy = JMX.newMBeanProxy(mbsc, getObjectName("ChatM"), IChatM.class, true);
        System.out.println(mbeanProxy.count());
    }
}
