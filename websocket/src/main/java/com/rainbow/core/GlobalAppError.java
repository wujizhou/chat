package com.rainbow.core;

/**
 * ErrorCode
 *
 * @author ybtccc
 * @date 2018/4/3
 */
public enum GlobalAppError {
    OK("00","操作成功"),
    NEED_LOGIN("01","需要登录"),
    NEED_REALNAME("02","需要实名"),
    ERROR("9999","系统错误");


    GlobalAppError(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    private String code;
    private String msg;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
