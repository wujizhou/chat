package com.rainbow.core;

/**
 * ErrorCode
 *
 * @author ybtccc
 * @date 2018/4/3
 */
public enum ErrorCode {
    MERCHANT_IS_NOT_EXIST("1000","商户不存在"),
    MERCHANT_DISCOUNT_IS_NOT_EXIST("1001","商户折扣不存在"),
    ACCOUNT_IS_NOT_ENOUGH("1002","余额不足"),

    GAME_ACT_ERROR_001("1003","活动不存在"),
    GAME_ACT_ERROR_002("1004","活动ID非法%s"),
    GAME_ACT_ERROR_003("1005","活动已过期%s"),
    GAME_ACT_ERROR_004("1006","活动还未开始%s"),
    GAME_ACT_ERROR_005("1007","活动未启用%s"),

    GAME_DICE_PLAY_PARAM_MONEY_ERROR("1008","下注元宝非法"),
    GAME_DICE_PLAY_PARAM_TYPE_ERROR("1009","下注类型非法"),
    GAME_DICE_PLAY_PARAM_ACCOUNT_NOT_ENOUGH("1010","余额不足"),

    GAME_DRAW_HAD_RUN_RESULT("1011","已结算"),
    GAME_DRAW_IS_NOT_END("1012","本轮未结束"),
    GAME_DRAW_NOT_AWARD("1013","未中"),
    GAME_DRAW_ERROR_PLAY("1014","获取牌非法"),

    GAME_SHOP_ERR_01("1015","商品不存在"),
    GAME_SHOP_ERR_02("1016","库存不足"),
    GAME_SHOP_ERR_03("1017","收货地址不能为空"),

    LOGIN_ERROR("1018","登录失败"),
    LOGIN_ERROR_LOCK("1019","账号被锁定"),

    GAME_SHOP_ADDR_OVER_LIMIT("1020","地址数量不能超过%s个"),
    GAME_SHOP_ADDR_NOT_EXIST("1021","地址不存在"),
    GAME_SHOP_ADDR_NOT_BELONG_TO_YOU("1022","用户地址匹配不正确"),



    ERROR("9999","系统错误");


    ErrorCode(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    private String code;
    private String msg;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
