package com.rainbow.constant;

import java.util.Random;

/**
 * 缓存key常量
 * @author 鲫鱼
 */
public class CacheConstants {

    /**
     * 头像列表
     */
    public static final String HEAD_IMG_LIST = "headImgList";

    /**
     * 用户在线
     */
    public static final String USER_ONLINE = "user_online";

    /**
     * 用户在线
     */
    public static final String USER_SESSION = "user_session";

    /**
     * 竞猜比赛,一场比赛一个缓存
     */
    public static final String GUESSING_COMPETITION = "GuessingCompetitionIng-";

    /**
     * 注册时redis锁
     */
    public static final String REDIS_LOCK_REGISTER = "lock_register";
    /**
     * 银行卡列表配置
     */
    public static final String CONFIG_BANKCARDLIST = "config_bankcardlist";
    /**
     * 页面模型信息 + page
     */
    public static final String PAGEMODULE = "pagemodule-";
    /**
     * 全部全局参数
     */
//    public static final String BASIC_PARAMS = "basic_params";
    /**
     * 最新的各类appVersion
     */
    public static final String APP_VERSION = "app_version";
    /**
     * 会员等级定义
     */
    public static final String USER_MEMBER_CONFIG = "user_member_config";
    /**
     * 某天的比赛列表,抓取的
     */
    public static final String SPIDER_DAILY_MATCH = "spider_daily_match-";
    /**
     * 全部比赛列表,抓取的,分页
     */
    public static final String SPIDER_MATCH = "spider_match_";
    /**
     * 某个资讯
     */
    public static final String SPIDER_NEWS = "spider_news-";

    /**
     * 是否开放发贴人申请
     */
    public static final String POST_APPLY = "postApply";

    /**
     * 发贴人申请时间间隔
     */
    public static final String POST_APPLY_DAYS = "postApplyDays";
    /**
     * 某个比赛的指数
     */
    public static final String SPIDER_MATCH_INDEX = "spider_match_index-";
    /**
     * 某个比赛主客队实力对比
     */
    public static final String SPIDER_MATCH_STRENGTH = "spider_match_strebgth- ";
    /**
     * 文字赛况
     */
    public static final String SPIDER_MATCH_SITUATION = "spider_match_situation-";
    /**
     * 赛事分析
     */
    public static final String SPIDER_MATCH_ANALYSIS = "spider_match_analysis-";
    /**
     * 队伍阵容
     */
    public static final String SPIDER_MATCH_LINEUP = "spider_match_lineup-";
    /**
     * 聊天室(文字直播?)
     */
    public static final String SPIDER_MATCH_LIVE = "spider_match_live-";

    /**
     * 临时发贴数
     */
    public static final String TEMP_POSTS = "tempPosts";

    /**
     * 每周
     *  - 周票数
     *  - 需分享数
     *  - 需签到天数
     */
    public static final String WEEK_SIGN_DAYS = "weekSignDays";
    public static final String WEEK_SHARE_NUM = "weekShareNum";


    /**
     * 当前未赛的足球联赛 A_Z分组 & 每组多少
     */
    public static final String SPIDER_MATCH_GROUP = "spider_match_group-";
    /**
     * 足球赛事全部联赛
     */
    public static final String SPIDER_MATCH_GROUP_ALL = "spider_match_group_all-";
    /**
     * 热门搜索词
     */
    public static final String SEARCH_HOT_WORDS = "search_hot_words";
    /**
     * 首页轮播图
     */
    public static final String INFO_CAROUSE_DIAGRAM = "info_carouse_diagram-";
    /**
     * 用户周任务信息
     */
    public static final String USER_WEEK_TASK_INFO = "user_week_task_info-";
    /**
     * 用户资讯信息 5s
     */
    public static String USER_ACCOUNT_INFO = "user_account_info-";
    /**
     * 用户是否关注了用户 5s
     */
//    public static String USER_ISFOCUS_USER = "user_isfocus_user-";

    /**
     * 用户基本信息
     */
    public static String NEWS_USER_INFO_DATA = "news:user:";

    /**
     * 购买Lock
     *  - 帖子/评论 post/comment
     *  - 会员 member
     *  - 锦囊 bag
     *  - 提现
     */
    public static final String PAY_FOR_POST   = "pay:post:";
    public static final String PAY_FOR_MEMBER = "pay:member:";
    public static final String PAY_FOR_BAG    = "pay:bag:";
    public static final String PAY_FOR_CASH   = "pay:cash:";
    /**
     * 反馈lock
     */
    public static final String APP_FAQ = "app:faq:";
    /**
     * 结算处理
     *  - 锦囊 bag
     */
    public static final String HANDLE_RESULT_BAG = "handle:result:bag:";
    /**
     * 自定义的数据缓存分钟,下面的依赖
     */
    public static final Integer CACHE_MINUTES = 5;
    /**
     * 大咖管理
     */
    public static String INFO_BIGNAME = "info_bigname-";
    /**
     * 修改手机号
     */
    public static final String UPDATE_PHONE = "update_phone-";
    /**
     * 发送短信
     */
    public static String SMSCODE      = "sms:code:";

    /**
     * 投票榜
     */
    public static String RANKING_VOTE   = "ranking:vote:%s_%s";
    public static String RANKING_WIN   = "ranking:win:%s_%s";
    public static String RANKING_POP   = "ranking:pop:%s_%s";
    public static String RANKING_RET   = "ranking:ret:%s_%s";

    /**
     * 获取缓存秒 , 防止自缓存数据同时失效同时查询db
     * @return
     */
    public static final Long getCacheSeconds(){
        return getCacheSeconds(CACHE_MINUTES);
    }
    public static final Long getCacheSeconds(Integer minutes){
        return minutes*60L+new Random().nextInt(10);
    }
}
