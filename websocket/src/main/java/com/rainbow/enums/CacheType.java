package com.rainbow.enums;


import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

/**
 * 缓存统一管理，name主要是注释用，value是缓存key的前缀
 *
 * @author Administrator
 */
public enum CacheType {
    GAME_FIRST_SEND_BD("GAME_FIRST_SEND_BD", "游戏首次送巴豆"),
    GAME_PMD_RECORD("GAME_PMD_RECORD", "游戏跑马灯记录"),
    CHOOSE_NINE_MATCH("CHOOSE_NINE_MATCH", "任选九某期比赛列表"),
    CHOOSE_NINE_MATCH_AMOUNT_RESULT("CHOOSE_NINE_MATCH_AMOUNT_RESULT", "任选九某期比赛单注中奖金额"),
    CHOOSE_NINE_MATCH_SCORE_RESULT("CHOOSE_NINE_MATCH_SCORE_RESULT", "任选九某期比赛比分"),
    USER_ACCOUNT_DETAIL_TYPE("USER_ACCOUNT_DETAIL_TYPE", "账户变动类型"),
    USER_ACCOUNT_DETAIL("USER_ACCOUNT_DETAIL", "用户明细- 详情"),
    USER_ACCOUNT_COUNT("USER_ACCOUNT_COUNT", "用户明细- 统计"),
    CHOOSE_NINE_LATEST_PERIOD("CHOOSE_NINE_LATEST_PERIOD", "任选九最新期数");

    private String value;
    private String name;

    CacheType(String value, String name) {
        this.value = value;
        this.name = name;
    }


    private static final Map<String, CacheType> lookup = new HashMap<String, CacheType>();

    static {
        for (CacheType s : EnumSet.allOf(CacheType.class)) {
            lookup.put(s.toValue(), s);
        }
    }

    /**
     * 获取枚举的值（整数值、字符串值等）
     */
    public String toValue() {
        return this.value;
    }

    public String toName() {
        return this.name;
    }


    /**
     * 根据值（整数值、字符串值等）获取相应的枚举类型
     */
    public static CacheType fromValue(String value) {
        return lookup.get(value);
    }
}
