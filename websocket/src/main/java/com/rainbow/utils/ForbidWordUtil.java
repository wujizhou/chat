package com.rainbow.utils;

import java.util.*;

/**
 * 敏感词处理工具 - DFA算法实现
 *
 * @author sam
 * @since 2017/9/4
 */
public class ForbidWordUtil {


    /**
     * 敏感词匹配规则
     * 最小匹配规则，如：敏感词库["中国","中国人"]，语句："我是中国人"，匹配结果：我是[中国]人
     * 最大匹配规则，如：敏感词库["中国","中国人"]，语句："我是中国人"，匹配结果：我是[中国人]
     */
    private static final int CONSTANT_ZERO      = 0;
    private static final int CONSTANT_ONE       = 1;
    private static final int CONSTANT_TWO       = 2;
    private static final String END_FLAG_KEY    = "isEnd";
    private static final String END_FLAG_ONE    = "1";
    private static final String END_FLAG_ZERO   = "0";

    /**
     * 敏感词集合，多层嵌套map
     */
    private static HashMap sensitiveWordMap = new HashMap();

    /**
     * 初始化敏感词库，构建DFA算法模型
     * @param sensitiveWordSet 敏感词库
     */
    private static synchronized void init(Set<String> sensitiveWordSet) {
        initSensitiveWordMap(sensitiveWordSet);
    }

    /**
     * 初始化敏感词库，构建DFA算法模型
     *
     * @param sensitiveWordSet 敏感词库
     */
    private static void initSensitiveWordMap(Set<String> sensitiveWordSet) {
        //初始化敏感词容器，减少扩容操作
        sensitiveWordMap = new HashMap(sensitiveWordSet.size());
        String key;
        //临时map
        Map nowMap;
        Map<String, String> newWorMap;
        for (String s : sensitiveWordSet) {
            key = s;
            nowMap = sensitiveWordMap;
            for (int i = CONSTANT_ZERO; i < key.length(); i++) {
                //转换成char型
                char keyChar = key.charAt(i);
                //库中获取关键字
                Object wordMap = nowMap.get(keyChar);
                //如果存在该key，直接赋值，用于下一个循环获取
                if (wordMap != null) {
                    nowMap = (Map) wordMap;
                } else {
                    //不存在则，则构建一个map，同时将isEnd设置为0，因为他不是最后一个
                    newWorMap = new HashMap<>(CONSTANT_TWO);
                    //不是最后一个
                    newWorMap.put(END_FLAG_KEY, END_FLAG_ZERO);
                    nowMap.put(keyChar, newWorMap);
                    nowMap = newWorMap;
                }

                if (i == key.length() - CONSTANT_ONE) {
                    //最后一个
                    nowMap.put(END_FLAG_KEY, END_FLAG_ONE);
                }
            }
        }

    }

    /**
     * 判断文字是否包含敏感字符
     *
     * @param txt       文字
     * @param matchType 匹配规则 1：最小匹配规则，2：最大匹配规则
     * @return 若包含返回true，否则返回false
     */
    public static boolean contains(String txt, int matchType) {
        boolean flag = false;
        for (int i = CONSTANT_ZERO; i < txt.length(); i++) {
            //判断是否包含敏感字符
            int matchFlag = checkSensitiveWord(txt, i, matchType);
            //大于0存在，返回true
            if (matchFlag > CONSTANT_ZERO) {
                flag = true;
                //只要检测到任何一个关键词就返回
                break;
            }
        }
        return flag;
    }

    /**
     * 判断文字是否包含敏感字符
     *
     * @param txt 文字
     * @return 若包含返回true，否则返回false
     */
    public static boolean contains(String txt) {
        return contains(txt, CONSTANT_TWO);
    }

    /**
     * 获取文字中的敏感词，应用场景，检测敏感词并提示给前端，建议使用最小匹配
     *
     * @param txt       文字
     * @param matchType 匹配规则 1：最小匹配规则，2：最大匹配规则
     * @return
     */
    public static Set<String> getSensitiveWord(String txt, int matchType) {
        Set<String> sensitiveWordList = new HashSet<>();

        for (int i = CONSTANT_ZERO; i < txt.length(); i++) {
            //判断是否包含敏感字符
            int length = checkSensitiveWord(txt, i, matchType);
            //存在,加入list中
            if (length > CONSTANT_ZERO) {
                sensitiveWordList.add(txt.substring(i, i + length));
                //减1的原因，是因为for会自增
                i = i + length - CONSTANT_ONE;
            }
        }

        return sensitiveWordList;
    }

    /**
     * 获取文字中的敏感词
     *
     * @param txt 文字
     * @return
     */
    public static Set<String> getSensitiveWord(String txt,Set<String> keyWordSet) {
        //如果传入的不是空set且与当前set长度不一致，重新初始化
        if (keyWordSet != null && keyWordSet.size() !=  sensitiveWordMap.size()){
            init(keyWordSet);
        }
        return getSensitiveWord(txt, CONSTANT_TWO);
    }

    /**
     * 替换敏感字字符
     *
     * @param txt         文本
     * @param replaceChar 替换的字符，匹配的敏感词以字符逐个替换，如 语句：我爱中国人 敏感词：中国人，替换字符：*， 替换结果：我爱***
     * @param matchType   敏感词匹配规则
     * @return
     */
    public static String replaceSensitiveWord(String txt, char replaceChar, int matchType) {
        String resultTxt = txt;
        //获取所有的敏感词
        Set<String> set = getSensitiveWord(txt, matchType);
        Iterator<String> iterator = set.iterator();
        String word;
        String replaceString;
        while (iterator.hasNext()) {
            word = iterator.next();
            replaceString = getReplaceChars(replaceChar, word.length());
            resultTxt = resultTxt.replaceAll(word, replaceString);
        }

        return resultTxt;
    }

    /**
     * 替换敏感字字符
     *
     * @param txt         文本
     * @param replaceChar 替换的字符，匹配的敏感词以字符逐个替换，如 语句：我爱中国人 敏感词：中国人，替换字符：*， 替换结果：我爱***
     * @return
     */
    public static String replaceSensitiveWord(String txt, char replaceChar) {
        return replaceSensitiveWord(txt, replaceChar, CONSTANT_TWO);
    }

    /**
     * 替换敏感字字符
     *
     * @param txt        文本
     * @param replaceStr 替换的字符串，匹配的敏感词以字符逐个替换，如 语句：我爱中国人 敏感词：中国人，替换字符串：[屏蔽]，替换结果：我爱[屏蔽]
     * @param matchType  敏感词匹配规则
     * @return String
     */
    public static String replaceSensitiveWord(String txt, String replaceStr, int matchType) {
        String resultTxt = txt;
        //获取所有的敏感词
        Set<String> set = getSensitiveWord(txt, matchType);
        Iterator<String> iterator = set.iterator();
        String word;
        while (iterator.hasNext()) {
            word = iterator.next();
            resultTxt = resultTxt.replaceAll(word, replaceStr);
        }

        return resultTxt;
    }

    /**
     * 替换敏感字字符
     *
     * @param txt        文本
     * @param replaceStr 替换的字符串，匹配的敏感词以字符逐个替换，如 语句：我爱中国人 敏感词：中国人，替换字符串：[屏蔽]，替换结果：我爱[屏蔽]
     * @return String
     */
    public static String replaceSensitiveWord(String txt, String replaceStr) {
        return replaceSensitiveWord(txt, replaceStr, CONSTANT_TWO);
    }

    /**
     * 获取替换字符串
     *
     * @param replaceChar char
     * @param length int
     * @return String
     */
    private static String getReplaceChars(char replaceChar, int length) {
        StringBuilder sb = new StringBuilder().append(replaceChar);
        for (int i = CONSTANT_ONE; i < length; i++) {
            sb.append(replaceChar);
        }
        return sb.toString();
    }

    /**
     * 检查文字中是否包含敏感字符，检查规则如下：<br>
     *
     * @param txt String
     * @param beginIndex int
     * @param matchType int
     * @return 如果存在，则返回敏感词字符的长度，不存在返回0
     */
    private static int checkSensitiveWord(String txt, int beginIndex, int matchType) {
        //敏感词结束标识位：用于敏感词只有1位的情况
        boolean flag = false;
        //匹配标识数默认为0
        int matchFlag = CONSTANT_ZERO;
        char word;
        Map nowMap = sensitiveWordMap;
        for (int i = beginIndex; i < txt.length(); i++) {
            word = txt.charAt(i);
            //获取指定key
            nowMap = (Map) nowMap.get(word);
            //存在，则判断是否为最后一个
            if (nowMap != null) {
                //找到相应key，匹配标识+1
                matchFlag++;
                //如果为最后一个匹配规则,结束循环，返回匹配标识数
                if (END_FLAG_ONE.equals(nowMap.get(END_FLAG_KEY))) {
                    //结束标志位为true
                    flag = true;
                    //最小规则，直接返回,最大规则还需继续查找
                    if (CONSTANT_ONE == matchType) {
                        break;
                    }
                }
            } else {
                //不存在，直接返回
                break;
            }
        }
        //长度必须大于等于1，为词
        if (matchFlag < CONSTANT_TWO || !flag) {
            matchFlag = CONSTANT_ZERO;
        }
        return matchFlag;
    }


    public static void main(String[] args) {

        Set<String> sensitiveWordSet = new HashSet<>();
        sensitiveWordSet.add("太多");
        sensitiveWordSet.add("爱恋");
        sensitiveWordSet.add("伤感情");
        sensitiveWordSet.add("伤感");
        sensitiveWordSet.add("静静");
        sensitiveWordSet.add("哈哈");
        sensitiveWordSet.add("啦啦");
        sensitiveWordSet.add("感动");
        sensitiveWordSet.add("发呆");
        //初始化敏感词库
        ForbidWordUtil.init(sensitiveWordSet);

        System.out.println("敏感词的数量：" + ForbidWordUtil.sensitiveWordMap.size());
        String string = "太多的伤感情怀也许只局限于饲养基地 荧幕中的情节。"
                + "然后我们的扮演的角色就是跟随着主人公的喜红客联盟 怒哀乐而过于牵强的把自己的情感也附加于银幕情节中，然后感动就流泪，"
                + "难过就躺在某一个人的怀里尽情的阐述心扉或者手机卡复制器一个贱人一杯红酒一部电影在夜 深人静的晚上，关上电话静静的发呆着。";
        System.out.println("待检测语句字数：" + string.length());

        //是否含有关键字
        boolean result = ForbidWordUtil.contains(string);
        System.out.println(result);
        result = ForbidWordUtil.contains(string, ForbidWordUtil.CONSTANT_ONE);
        System.out.println(result);

        //获取语句中的敏感词
        Set<String> set = ForbidWordUtil.getSensitiveWord(string,null);
        System.out.println("语句中包含敏感词的个数为：" + set.size() + "。包含：" + set);
        set = ForbidWordUtil.getSensitiveWord(string, ForbidWordUtil.CONSTANT_ONE);
        System.out.println("语句中包含敏感词的个数为：" + set.size() + "。包含：" + set);

        //替换语句中的敏感词
        String filterStr = ForbidWordUtil.replaceSensitiveWord(string, '*');
        System.out.println(filterStr);
        filterStr = ForbidWordUtil.replaceSensitiveWord(string, '*', ForbidWordUtil.CONSTANT_ONE);
        System.out.println(filterStr);

        String filterStr2 = ForbidWordUtil.replaceSensitiveWord(string, "[*敏感词*]");
        System.out.println(filterStr2);
        filterStr2 = ForbidWordUtil.replaceSensitiveWord(string, "[*敏感词*]", ForbidWordUtil.CONSTANT_ONE);
        System.out.println(filterStr2);
    }

}