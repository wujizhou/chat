package com.rainbow.utils;

import org.apache.commons.lang3.StringUtils;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.*;

public class RequestUtil {
		public static StringBuffer makeParams(HttpServletRequest request){
			StringBuffer temp=new StringBuffer();
			temp.append(request.getRequestURL());
			Enumeration<?> parameterNames=request.getParameterNames();
			if(parameterNames != null &&  parameterNames.hasMoreElements()){
				while(parameterNames.hasMoreElements()){
					Object name=parameterNames.nextElement();
					if(temp.toString().contains("?")){
						temp.append("&");
					}else{
						temp.append("?");
					}
					temp.append(name);
					temp.append("=");
					temp.append(request.getParameter(name.toString()));
				}
			}
			return temp;
		}
		public static String getParamValue(String url,String key){
			URI uri;
			try {
				uri = new URI(url);
				String query=uri.getQuery();
				String []arr=query.split("&");
				for(int i=0;i<arr.length;i++) {
					String temp=arr[i];
					int index=temp.indexOf("=");
					if(index>0) {
						String sKey=temp.substring(0,index);
						if(sKey.equals(key)){
							return temp.substring(index+1);
						}
					}
				}
			} catch (URISyntaxException e) {
				e.printStackTrace();
			}
			
			return "";
		}
		
		public static LinkedHashMap<String,String> getLinkedParams(HttpServletRequest request) {  
	        LinkedHashMap<String,String> map = new LinkedHashMap<String,String>();  
	        List<String> keys = new ArrayList(request.getParameterMap().keySet());
	        for(String key:keys) {  
	            String[] paramValues = request.getParameterValues(key); 
	            if(paramValues.length > 0){
	            	String paramValue = "";
	            	for(int i = 0; i < paramValues.length;i++){
	            		String paramTemp = paramValues[i];
	                	if (StringUtils.isNotBlank(paramTemp)) {
	                		paramValue += paramTemp;
	                    }  
	                }
	            	map.put(key, paramValue);  
	            }
	        }  
	        return map;
	    }  
		
		/** 
		* @Title: callBackParms 
		* @Description: TODO <获取回调参数MAP>
		* @param @param request
		* @param @return 参数集合 Map<String,String>,订单编号 out_trade_no,支付宝交易号 trade_no
		* @param @throws IOException
		* @param @throws AlipayApiException <参数说明>
		* @return Map<String,String> 
		* @throws 
		*/
		public static Map<String,String> getParmsMap(HttpServletRequest request) throws IOException{
			Map<String,String> params = new TreeMap<String,String>();
			Map requestParams = request.getParameterMap();
			for (Iterator iter = requestParams.keySet().iterator(); iter.hasNext();) {
				String name = (String) iter.next();
				String[] values = (String[]) requestParams.get(name);
				String valueStr = "";
				for (int i = 0; i < values.length; i++) {
					valueStr = (i == values.length - 1) ? valueStr + values[i]
							: valueStr + values[i] + ",";
				}
//				乱码解决，这段代码在出现乱码时使用。如果mysign和sign不相等也可以使用这段代码转化
//				valueStr = new String(valueStr.getBytes("ISO-8859-1"), "utf-8");
				params.put(name, valueStr);
			}
			return params;
		}
}
