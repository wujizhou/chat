package com.rainbow.utils;

import javax.servlet.http.HttpServletRequest;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.UnknownHostException;
import java.util.Enumeration;
import java.util.List;

public class IpUtil {
	public static String getIp(HttpServletRequest request) {
// remoteIp
		String ip = request.getHeader("x-forwarded-for");
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("WL-Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("HTTP_CLIENT_IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("HTTP_X_FORWARDED_FOR");
		}
		if (StringUtil.isEmpty(ip)) {
			ip = request.getHeader("Client_IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getRemoteAddr();
			if ("127.0.0.1".equals(ip) || "0:0:0:0:0:0:0:1".equals(ip)) {
				// 根据网卡取本机配置的IP
				InetAddress inet = null;
				try {
					inet = InetAddress.getLocalHost();
				} catch (UnknownHostException e) {
					e.printStackTrace();
				}
				ip = inet.getHostAddress();
			}
		}
		if (!StringUtil.isEmpty(ip)) {
			List<String> list = StringUtil.stringToList(ip, ",");
			if (list.size() > 1) {
				ip = list.get(0);
			}
		}

		return ip;
	}

	/**
	 * 
	 * 获得主机IP
	 *
	 * 
	 * 
	 * @return String
	 * 
	 */

	public static boolean isWindowsOS() {

		boolean isWindowsOS = false;

		String osName = System.getProperty("os.name");

		if (osName.toLowerCase().indexOf("windows") > -1) {

			isWindowsOS = true;

		}

		return isWindowsOS;

	}

	/**
	 * 
	 * 获取本机ip地址，并自动区分Windows还是linux操作系统
	 * 
	 * @return String
	 * 
	 */

	public static String getLocalIP() {

		String sIP = "";

		InetAddress ip = null;

		try {

			// 如果是Windows操作系统

			if (isWindowsOS()) {

				ip = InetAddress.getLocalHost();

			}

			// 如果是Linux操作系统

			else {

				boolean bFindIP = false;

				Enumeration<NetworkInterface> netInterfaces = (Enumeration<NetworkInterface>) NetworkInterface

						.getNetworkInterfaces();

				while (netInterfaces.hasMoreElements()) {

					if (bFindIP) {

						break;

					}

					NetworkInterface ni = (NetworkInterface) netInterfaces.nextElement();

					// ----------特定情况，可以考虑用ni.getName判断

					// 遍历所有ip

					Enumeration<InetAddress> ips = ni.getInetAddresses();

					while (ips.hasMoreElements()) {

						ip = (InetAddress) ips.nextElement();

						if (ip.isSiteLocalAddress()

								&& !ip.isLoopbackAddress() // 127.开头的都是lookback地址

								&& ip.getHostAddress().indexOf(":") == -1) {

							bFindIP = true;

							break;

						}

					}

				}

			}

		}

		catch (Exception e) {

			e.printStackTrace();

		}

		if (null != ip) {

			sIP = ip.getHostAddress();

		}

		return sIP;

	}
}
