package com.rainbow.utils;

import com.aliyun.oss.ClientException;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.http.MethodType;
import com.aliyuncs.http.ProtocolType;
import com.aliyuncs.profile.DefaultProfile;
import com.aliyuncs.profile.IClientProfile;
import com.aliyuncs.sts.model.v20150401.AssumeRoleRequest;
import com.aliyuncs.sts.model.v20150401.AssumeRoleResponse;

import java.util.HashMap;
import java.util.Map;

public class STSUtil {
    public static Map<String, Object> getResponse() {
        Map<String, Object> map = new HashMap<>();
        String endpoint = "sts.aliyuncs.com";
        String accessKeyId = "LTAIinKLpQwQPs1H";
        String accessKeySecret = "HqIY8AKryHDKBwFMACUxiMcMF66cuD";
        String roleArn = "acs:ram::1132051259301248:role/ramappreadonly";
        //roleSessionName时临时Token的会话名称，自己指定用于标识你的用户，或者用于区分Token颁发给谁
        //要注意roleSessionName的长度和规则，不要有空格，只能有'-'和'_'字母和数字等字符
        String roleSessionName = "session-name";
        String policy = null;
        ProtocolType protocolType = ProtocolType.HTTPS;
        try {
            DefaultProfile.addEndpoint("", "", "Sts", endpoint);
            IClientProfile profile = DefaultProfile.getProfile("", accessKeyId, accessKeySecret);
            DefaultAcsClient client = new DefaultAcsClient(profile);
            final AssumeRoleRequest request = new AssumeRoleRequest();
            request.setMethod(MethodType.POST);
            request.setRoleArn(roleArn);
            request.setRoleSessionName(roleSessionName);
            request.setPolicy(policy);
            request.setProtocol(protocolType);
            final AssumeRoleResponse response = client.getAcsResponse(request);
//            System.out.println("=============" + response.getCredentials().getAccessKeyId());
//            System.out.println("=============" + response.getCredentials().getAccessKeySecret());
//            System.out.println("=============" + response.getCredentials().getSecurityToken());
            map.put("accessKeyId",response.getCredentials().getAccessKeyId());
            map.put("accessKeySecret",response.getCredentials().getAccessKeySecret());
            map.put("securityToken",response.getCredentials().getSecurityToken());
            map.put("endPoint","http://oss-cn-hangzhou.aliyuncs.com");
            map.put("bucketName","zhongimage");
        } catch (ClientException | com.aliyuncs.exceptions.ClientException e) {
            e.printStackTrace();
        }
        return map;
    }
}
