package com.rainbow.chat.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.rainbow.chat.enums.ChatMsgType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 聊天信息 db实体
 * @author rainbow
 */
@Data
@TableName
@ApiModel("聊天信息实体类")
public class InfoChatMsg implements Serializable {
    @TableId(type= IdType.AUTO)
    private Integer id;
    @ApiModelProperty("内容")
    private String  msg;
    @ApiModelProperty("接受者0,1+")
    private Integer toUid;
    @ApiModelProperty("群组id")
    private Integer groupId;
    @ApiModelProperty("消息大类")
    private ChatMsgType type;
    @ApiModelProperty("消息小类")
    private ChatMsgType typeSmall;
    @ApiModelProperty("发送者 0,1+")
    private Integer fromUid;
    @ApiModelProperty("发送时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime time;
    @ApiModelProperty("回话ID")
    private String chatIndex;

//    @TableField
//    private LocalDateTime createTime;
    @ApiModelProperty("消息id")
    @TableField(exist = false)
    private String msgId;
    @ApiModelProperty("接收者名字")
    @TableField(exist = false)
    private Integer toUname;
    @ApiModelProperty("接收者头像")
    @TableField(exist = false)
    private Integer toUavatar;
    @ApiModelProperty("发送者名字")
    @TableField(exist = false)
    private Integer fromUname;
    @ApiModelProperty("发送者头像")
    @TableField(exist = false)
    private Integer fromUavatar;
}
