package com.rainbow.chat.config;

import com.rainbow.chat.cache.RedisCommonMethod;
import com.rainbow.chat.cache.WbRedisClientTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import redis.clients.jedis.JedisPool;

@Configuration
public class RedisConfig {

    @Value("${spring.redis.host}")
    private String hostName;

    @Value("${spring.redis.port}")
    private Integer port;

    @Value("${spring.redis.password}")
    private String password;

    @Value("${spring.redis.jedis.pool.max-idle}")
    private Integer maxIdle;

    @Value("${spring.redis.jedis.pool.min-idle}")
    private Integer minIdle;

    @Value("${spring.redis.jedis.pool.max-active}")
    private Integer maxActive;

    @Value("${spring.redis.jedis.pool.max-wait}")
    private Long maxWait;

    @Value("${spring.redis.timeout}")
    private Long timeOut;

    @Value("${spring.redis.database}")
    private Integer database;

    @Bean
    public WbRedisClientTemplate getRedis() {
        return new WbRedisClientTemplate(RedisCommonMethod.getRedisTemplateByIndex(
                hostName,port,password,maxIdle,maxActive,maxWait,database!=null ? database : 0));
    }

    @Bean(name = "jedisPoolPrivate")
    public JedisPool getJedisPool() {
        return new JedisPool(RedisCommonMethod.poolConfig(maxIdle,maxActive,maxWait),hostName,port, Math.toIntExact(timeOut),password);
    }
}
