package com.rainbow.chat.config;

import com.rainbow.chat.enums.GlobalAppError;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.builders.ResponseMessageBuilder;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ResponseMessage;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author 鲫鱼
 */
@Configuration
@EnableSwagger2
public class WebMvcConfig extends WebMvcConfigurationSupport {

    private static final String PROD = "prod";

    @Value("${spring.profiles.active}")
    private String demode;

    @Bean
    public Docket createRestApi() {
        if (!PROD.equals(demode)){
            //  添加全局响应状态码
            List<ResponseMessage> responseMessageList = new ArrayList<>();
            Arrays.stream(GlobalAppError.values()).forEach(errorEnum -> {
                responseMessageList.add(
                        new ResponseMessageBuilder()
                                .code(errorEnum.getCode()).message(errorEnum.getDesc())
//                                .responseModel(new ModelRef(errorEnum.getDesc()))
                                .build()
                );
            });
            return new Docket(DocumentationType.SWAGGER_2).groupName("聊天系统")
//                    .forCodeGeneration(true)
                    .useDefaultResponseMessages(false)
                    .select().apis(RequestHandlerSelectors.basePackage("com.rainbow.chat"))
                    .paths(PathSelectors.any())
                    .build().apiInfo(apiInfo())
//                    .pathMapping("/dev-api")
                    // 添加全局响应状态码
                    .globalResponseMessage(RequestMethod.GET, responseMessageList)
                    .globalResponseMessage(RequestMethod.POST, responseMessageList)
                    ;
        }else {
            return new Docket(null);
        }
    }

    /**构建 api文档的详细信息函数,注意这里的注解引用的是哪个*/
    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("聊天系统对接 API")
                .version("1.0")
                .description("http+websocket聊天")
                .build();
    }

    @Override
    protected void addResourceHandlers(ResourceHandlerRegistry registry) {
        if (!PROD.equals(demode)){
            registry.addResourceHandler("/swagger-ui.html").addResourceLocations(
                    "classpath:/META-INF/resources/");
            registry.addResourceHandler("/webjars/**").addResourceLocations(
                    "classpath:/META-INF/resources/webjars/");
        }
    }
//
//    @Bean
//    public FastJsonConfig fastJsonConfig() {
//        FastJsonConfig fastJsonConfig = new FastJsonConfig();
//        fastJsonConfig.setSerializeFilters((ValueFilter) (Object object, String name, Object value) -> {
//            if (value instanceof Double) {
//                return value.toString();
//            } else {
//                return value;
//            }
//        });
//        return fastJsonConfig;
//    }
}
