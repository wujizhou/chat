package com.rainbow.chat.init;

import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

@Component
//@Order(1)
public class Init {
    @PostConstruct
    private void init(){
        // TODO 查询过滤池
        // is8n
        // ...
    }

    @PreDestroy
    private void crash(){
    }
}
