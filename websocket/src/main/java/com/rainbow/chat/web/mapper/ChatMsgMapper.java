package com.rainbow.chat.web.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.rainbow.chat.entity.InfoChatMsg;

public interface ChatMsgMapper extends BaseMapper<InfoChatMsg> {
}
