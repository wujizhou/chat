package com.rainbow.chat.web.controller;

import com.rainbow.chat.entity.InfoChatMsg;
import com.rainbow.chat.ws.WsServer;
import com.rainbow.chat.web.service.ChatMsgService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/msg")
@Api(value = "聊天/推送",tags = "msg tags",description = "msg desc")
public class MsgController {
    @Autowired
    private ChatMsgService msgService;
    @Autowired
    private WsServer wsServer;

    @GetMapping("user")
    @ResponseBody
    @ApiOperation("单推")
    public void user(@RequestParam Integer uid){
        wsServer.pushUser(uid,"user");
    }

    @GetMapping("queryMsg/{chatIndex}")
    @ResponseBody
    @ApiOperation("消息列表")
    public List<InfoChatMsg> queryMsg(@PathVariable String chatIndex){
        return msgService.queryPage(chatIndex,0,10);
    }

    @GetMapping("group")
    @ResponseBody
    @ApiOperation("群聊")
    public void group(@RequestParam Integer groupId){
        wsServer.pushGroup(groupId,"group");
    }

    @GetMapping("cast")
    @ResponseBody
    @ApiOperation("广播")
    public void cast(){
        wsServer.pushAll("cast ");
    }
}
