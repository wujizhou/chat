package com.rainbow.chat.web.controller;

import com.rainbow.chat.cache.WbRedisClientTemplate;
import com.rainbow.chat.jmx.IChatM;
import com.rainbow.chat.jmx.IRedisM;
import com.rainbow.constant.CacheConstants;
import com.rainbow.exception.BusinessException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.management.JMX;
import javax.management.MBeanServerConnection;
import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;
import java.io.IOException;

@Controller
@RequestMapping("/jmx")
@Api(value = "管理",tags = "管理tags",description = "管理desc")
public class JmxController {
    public static final String PACKAGE = "com.rainbow.chat.jmx:type=";
    public static final String URL = ":9008";
    @Autowired
    private WbRedisClientTemplate<String,String> redis;

    @GetMapping("test")
    @ResponseBody
    public String test(@RequestParam Integer uid) throws BusinessException {
        return redis.get(CacheConstants.USER_SESSION + uid);
    }


    private JMXConnector jmxc;
    @PreDestroy
    private void crash() throws IOException {
        jmxc.close();
    }
    @PostConstruct
    private void init() throws IOException {
        // *** VM options ***
        // -Dcom.sun.management.jmxremote.port=9008
        // -Dcom.sun.management.jmxremote.authenticate=false
        // -Dcom.sun.management.jmxremote.ssl=false
        JMXServiceURL url =
                new JMXServiceURL("service:jmx:rmi:///jndi/rmi://"+ URL +"/jmxrmi");
        jmxc = JMXConnectorFactory.connect(url, null);
    }

    private ObjectName getObjectName(String name) throws MalformedObjectNameException {
        return new ObjectName(PACKAGE+name);
    }
    @GetMapping("jmx")
    @ResponseBody
    @ApiOperation("jmx管理")
    public String jmx() throws IOException, MalformedObjectNameException {
        MBeanServerConnection mbsc = jmxc.getMBeanServerConnection();
        // Create a dedicated proxy for the MBean instead of going directly through the MBean server connection
        IChatM mbeanProxy = JMX.newMBeanProxy(mbsc, getObjectName("ChatM"), IChatM.class, true);
        mbeanProxy.count();
        return mbeanProxy.getCount();
    }


    @GetMapping("redis/{database}/{key}")
    @ResponseBody
    @ApiOperation("redis管理")
    public String redis(
            @PathVariable Integer database,@PathVariable String key
    ) throws IOException,
            MalformedObjectNameException, BusinessException {
        MBeanServerConnection mbsc = jmxc.getMBeanServerConnection();
        // Create a dedicated proxy for the MBean instead of going directly through the MBean server connection
        IRedisM mbeanProxy = JMX.newMBeanProxy(mbsc, getObjectName("RedisM"), IRedisM.class, true);
        return mbeanProxy.redisSelect(database,key);
    }
}
