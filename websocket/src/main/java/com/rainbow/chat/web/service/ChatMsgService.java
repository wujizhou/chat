package com.rainbow.chat.web.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.rainbow.chat.entity.InfoChatMsg;
import com.rainbow.chat.web.mapper.ChatMsgMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ChatMsgService {

    @Autowired
    private ChatMsgMapper mapper;

    public List<InfoChatMsg> queryPage(String chatIndex, int page, int size){
        QueryWrapper<InfoChatMsg> wrapper = new QueryWrapper<>();
        wrapper.eq("chat_index",chatIndex)
//                .and(w->w.eq("from_uid",uid).or().eq("to_uid",uid))
                .orderByDesc("time");
        IPage ipage = mapper.selectPage(new Page<>(page,size,false),wrapper);
        return ipage.getRecords();
    }

    public Integer add(InfoChatMsg chatMsg) {
        return mapper.insert(chatMsg);
    }
}
