package com.rainbow.chat.test.socket;


import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.DelimiterBasedFrameDecoder;
import io.netty.handler.codec.Delimiters;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;
import io.netty.handler.timeout.IdleStateHandler;

import java.util.concurrent.TimeUnit;

/**
 * @Author: Usher
 * @Description:
 */
public class SocketClientInit extends ChannelInitializer<SocketChannel> {

    @Override
    protected void initChannel(SocketChannel socketChannel) throws Exception {
        ChannelPipeline pipeline = socketChannel.pipeline();
        pipeline.addLast("frame",new DelimiterBasedFrameDecoder(8192, Delimiters.lineDelimiter()))
                .addLast("decode",new StringDecoder()) //解码器
                .addLast("ping",new IdleStateHandler(20,10,0, TimeUnit.SECONDS))
                .addLast("encode",new StringEncoder())
                .addLast("handler",new SocketClientHandler())
        ;
    }
}
