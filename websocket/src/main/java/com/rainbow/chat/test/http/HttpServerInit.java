package com.rainbow.chat.test.http;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.http.HttpContentCompressor;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.HttpRequestDecoder;
import io.netty.handler.codec.http.HttpResponseEncoder;

public class HttpServerInit extends ChannelInitializer<SocketChannel> {
    @Override
    protected void initChannel(SocketChannel ch) throws Exception {
        ChannelPipeline pipeline = ch.pipeline();
        pipeline.addLast("responseEncode", new HttpResponseEncoder());
        pipeline.addLast("requestDecode", new HttpRequestDecoder());

        pipeline.addLast("objectAggregator", new HttpObjectAggregator(1024));
        pipeline.addLast("contentCompressor", new HttpContentCompressor());

        pipeline.addLast("httpServerHandler", new HttpServerHandler());
    }
}