package com.rainbow.chat.cache;


import com.rainbow.enums.CacheType;

import java.io.Serializable;


/**
 * 缓存统一管理对象  cachePre 缓存前缀主要是用于区分业务，防止冲突，key对应缓存的key
 * @author Administrator
 *
 * @param <T>
 */
public class CacheEntity<T> implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private CacheType cachePre;
	private T key;
	
	public CacheEntity(T key, CacheType cachePre){
		this.cachePre = cachePre;
		this.key = key;
	}
	
	public T getKey() {
		return key;
	}
	public void setKey(T key) {
		this.key = key;
	}

	public CacheType getCachePre() {
		return cachePre;
	}

	public void setCachePre(CacheType cachePre) {
		this.cachePre = cachePre;
	}

    @Override
    public String toString() {
	    return getCachePre().toValue() + getKey().toString();
    }
}
