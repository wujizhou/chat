package com.rainbow.chat.cache;

import com.rainbow.exception.BusinessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

/**
 * @author ybtccc
 * @date 2018/1/29
 */
@Component
public class RedisLock {
    private static Logger logger = LoggerFactory.getLogger(RedisLock.class);

    private static JedisPool jedisPool;

    @Autowired
    public RedisLock(JedisPool jedisPool){
        RedisLock.jedisPool = jedisPool;
    }

    public static void addLock(String key, long timeout) throws BusinessException {
        logger.info("addLock：key" + key + ",timeout:" + timeout);
        try (Jedis jedis = jedisPool.getResource()) {
            String resp = jedis.set("com.zhuoyue.lock." + key, "1", "nx", "ex", timeout);
            if (resp == null) {
                throw new BusinessException("99999", "亲，操作太频繁了，请" + timeout + "秒后再试");
            }
        }
    }

    public static void removeLock(String key) {
        try (Jedis jedis = jedisPool.getResource()) {
            jedis.del("com.rainbow.lock." + key);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }
}
