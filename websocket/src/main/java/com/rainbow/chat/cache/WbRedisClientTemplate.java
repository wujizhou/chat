package com.rainbow.chat.cache;

import com.rainbow.exception.BusinessException;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;

import java.util.Set;
import java.util.concurrent.TimeUnit;


/**
 * 如需要使用家入xml配置
 * @author 67339
 *
 * @param <K>
 * @param <V>
 */
public class WbRedisClientTemplate<K,V> {
	private static final Logger LOG = LoggerFactory.getLogger(WbRedisClientTemplate.class);
	private static RedisTemplate redisTemplate;
	@Value("${spring.redis.database}")
	private Integer db;

	public WbRedisClientTemplate(RedisTemplate redisTemplate) {
		WbRedisClientTemplate.redisTemplate = redisTemplate;
	}

	public void select(int database){
		if(this.db != database){
			((JedisConnectionFactory) redisTemplate.getConnectionFactory()).setDatabase(database);
			this.db = database;
		}
	}

	/**
	 * 此方法不建议用
	 */
	public void put(K key,V value,long expTime,TimeUnit unit) {
		if (key == null || value == null) {
			LOG.error("WbRedisClientTemplate.put error", "缓存key/value不能为空");
			return;
		}
		try {
			ValueOperations<K,V> s = (ValueOperations<K, V>) redisTemplate.opsForValue();
			s.set(key, value,expTime,unit);
		} catch (Exception e) {
			LOG.error("WbRedisClientTemplate.put error", e);
		}
	}

	public  V  get(K key) throws BusinessException {
		if(key==null){
			throw new BusinessException("ERR_CACHE_001", "缓存key不能为空");
		}
		try {
			ValueOperations<K,V> s =  (ValueOperations<K, V>) redisTemplate.opsForValue();
			return s.get(key);
		} catch (Exception e) {
			LOG.error("WbRedisClientTemplate.get error", e);
		}
		return null;
	}

	/**
	 * 存放缓存
	 * @param key {@link CacheEntity} 用法：new CacheEntity(xxxx,CacheType.xxx)
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public  void put(CacheEntity key, V value, long expTime, TimeUnit unit) throws BusinessException {
		if(key==null||key.getKey()==null|| StringUtils.isEmpty(key.getKey().toString())){
			throw new BusinessException("ERR_CACHE_001", "缓存key不能为空");
		}
		if(value==null){
			throw new BusinessException("ERR_CACHE_002", "缓存内容不能为空");
		}
		try {
			ValueOperations<String,V> s = (ValueOperations<String, V>) redisTemplate.opsForValue();
			String keyI = key.getCachePre().toValue()+key.getKey().toString();
			s.set(keyI, value,expTime,unit);
		} catch (Exception e) {
			LOG.error("WbRedisClientTemplate.put error", e);
		}
	}

	/**
	 * 获取缓存
	 * @param key {@link CacheEntity} 用法：new CacheEntity(xxxx,CacheType.xxx)
	 */
	public  V  get(@SuppressWarnings("rawtypes") CacheEntity key) throws BusinessException {
		if(key==null||key.getKey()==null||StringUtils.isEmpty(key.getKey().toString())){
			throw new BusinessException("ERR_CACHE_001", "缓存key不能为空");
		}
		try {
			ValueOperations<K,V> s = redisTemplate.opsForValue();
			String keyI = key.getCachePre().toValue()+key.getKey().toString();
			return s.get(keyI);
		} catch (Exception e) {
			LOG.error("WbRedisClientTemplate.get error", e);
		}
		return null;
	}

	/**
	 * 删除缓存
	 * @param key {@link CacheEntity} 用法：new CacheEntity(xxxx,CacheType.xxx)
	 */
	public void delete(String key) throws BusinessException {
		if(key==null){
			throw new BusinessException("ERR_CACHE_001", "缓存key不能为空");
		}
		try {
			ValueOperations<String,Object> s = (ValueOperations<String, Object>) redisTemplate.opsForValue();
			RedisOperations<String, Object> operations = s.getOperations();
			operations.delete(key);
		} catch (Exception e) {
			LOG.error("WbRedisClientTemplate.put error", e);
		}
	}

	/**
	 * 批量删除缓存
	 * @param key {@link CacheEntity} 用法：new CacheEntity(xxxx,CacheType.xxx)
	 */
	@SuppressWarnings("unchecked")
	public void batchDelete(String key) throws BusinessException {
		if(key==null){
			throw new BusinessException("ERR_CACHE_001", "缓存key不能为空");
		}
		try {
			ValueOperations<String,Object> s = (ValueOperations<String, Object>) redisTemplate.opsForValue();
			RedisOperations<String, Object> operations = s.getOperations();
			Set keys = redisTemplate.keys(key + "*");
			if (keys !=null && keys.size() > 0) {
				operations.delete(keys);
			}
		} catch (Exception e) {
			LOG.error("WbRedisClientTemplate.put error", e);
		}
	}

	/**
	 * keys *
	 */
	public int getKeysCount(String pattern) throws BusinessException {

		try {
			return redisTemplate.keys(pattern).size();
		} catch (Exception e) {
			LOG.error("WbRedisClientTemplate.put error", e);
		}
		return 0;
	}
}