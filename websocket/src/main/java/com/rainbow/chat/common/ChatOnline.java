package com.rainbow.chat.common;

import io.netty.channel.ChannelId;
import lombok.Data;

import java.io.Serializable;

@Data
public class ChatOnline implements Serializable {
    private ChannelId channelId;
    private Integer group = 0;
    private String sid;

    @Override
    public String toString(){
        return channelId + ":" + group + ":" + sid;
    }
}
