package com.rainbow.chat.common;

import com.alibaba.fastjson.JSON;
import com.rainbow.chat.enums.ChatMsgType;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;

public class ChatUtil {
    public static final Integer SYSTEM = -1; //系统 user/..
    public static final Integer ALL    = 0; //全部 group/user/..

    /**
     * @param msg
     * @return
     */
    public static TextWebSocketFrame getMsg(Object msg){
        return new TextWebSocketFrame(JSON.toJSONString(msg));
//        return JSON.toJSONString(msg) + "\n";
    }

    public static TextWebSocketFrame getChatMsg(Integer fromUid,Integer toUid,String msg){
        ChatMsg tmp = new ChatMsg();
        tmp.setMsg(msg);
        tmp.setFromUid(fromUid);
        tmp.setToUid(toUid);
        tmp.setType(ChatMsgType.CHAT);
        return getMsg(tmp);
    }

    public static TextWebSocketFrame getGroupMsg(Integer fromUid,Integer groupId,String msg){
        ChatMsg tmp = new ChatMsg();
        tmp.setMsg(msg);
        tmp.setFromUid(fromUid);
        tmp.setGroupId(groupId);
        tmp.setType(ChatMsgType.CHAT);
        return getMsg(tmp);
    }

    public static TextWebSocketFrame getBoardMsg(Integer groupId,String msg){
        ChatMsg tmp = new ChatMsg();
        tmp.setMsg(msg);
        tmp.setGroupId(groupId);
        tmp.setType(ChatMsgType.BOARDCAST);
        return getMsg(tmp);
    }

    public static TextWebSocketFrame getErrMsg(String msg){
        ChatMsg tmp = new ChatMsg();
        tmp.setMsg(msg);
        tmp.setType(ChatMsgType.ERROR);
        return getMsg(tmp);
    }

    public static TextWebSocketFrame getTipMsg(String msg){
        ChatMsg tmp = new ChatMsg();
        tmp.setMsg(msg);
        return getMsg(tmp);
    }

    public static TextWebSocketFrame getTipMsg(String msg,ChatMsgType type){
        ChatMsg tmp = new ChatMsg();
        tmp.setMsg(msg);
//        tmp.setTypeSmall(type);
        return getMsg(tmp);
    }
}
