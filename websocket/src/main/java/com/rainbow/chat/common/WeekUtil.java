package com.rainbow.chat.common;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by ybtccc on 2017/12/21.
 * WeekUtil
 */
public enum WeekUtil {
    Mon("1", "周一"),
    Tue("2", "周二"),
    Wed("3", "周三"),
    Thu("4", "周四"),
    Fri("5", "周五"),
    Sat("6", "周六"),
    Sun("7", "周日");
    private String num;
    private String week;

    public static Map<String, WeekUtil> keyMap = new HashMap<>();
    public static Map<String, WeekUtil> valueMap = new HashMap<>();

    static {
        for(WeekUtil week : EnumSet.allOf(WeekUtil.class)){
            keyMap.put(week.getNum(),week);
            valueMap.put(week.getWeek(),week);
        }
    }

    WeekUtil() {
    }

    WeekUtil(String num, String week) {
        this.num = num;
        this.week = week;
    }

    public String getNum() {
        return num;
    }

    public void setNum(String num) {
        this.num = num;
    }

    public String getWeek() {
        return week;
    }

    public void setWeek(String week) {
        this.week = week;
    }
}
