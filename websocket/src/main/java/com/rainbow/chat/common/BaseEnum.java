package com.rainbow.chat.common;

import com.alibaba.fastjson.serializer.JSONSerializable;
import com.alibaba.fastjson.serializer.JSONSerializer;

import java.io.IOException;
import java.lang.reflect.Type;

public interface BaseEnum extends JSONSerializable {
    Integer getCode();
    String getDesc();
    @Override
    default void write(JSONSerializer serializer, Object fieldName, Type fieldType,
                       int features) throws IOException {
        serializer.write(getCode());
    }
    @Override
    String toString();
}
