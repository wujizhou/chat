package com.rainbow.chat.common;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.rainbow.chat.enums.ChatMsgType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 消息实体 从前端接收到的
 * @author rainbow
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("接收消息")
public class ChatMsg implements Serializable {
    private String  msg;

    private ChatMsgType type = ChatMsgType.TIP;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private ChatMsgType typeSmall = ChatMsgType.ERROR;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime time = LocalDateTime.now();
    @ApiModelProperty("消息唯一键")
    private String  msgId;
    @ApiModelProperty("发送者 0,1+")
    private Integer fromUid  = ChatUtil.SYSTEM;
    private String  fromUname;
    private String  fromAvatar;

    @ApiModelProperty("接受者0,1+")
    private Integer toUid    = ChatUtil.ALL;
    @ApiModelProperty("群组id")
    private Integer groupId  = ChatUtil.ALL;


}
