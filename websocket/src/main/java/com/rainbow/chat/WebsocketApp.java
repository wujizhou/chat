package com.rainbow.chat;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.rainbow.chat.web.mapper")
public class WebsocketApp {

    public static void main(String[] args) {
        SpringApplication.run(WebsocketApp.class, args);
    }
}