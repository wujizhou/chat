package com.rainbow.chat.enums;

import com.baomidou.mybatisplus.core.enums.IEnum;


public enum UserStatus implements IEnum<Integer> {
    ONLINE(1,"online")
    ,OFFLINE(0,"offline")
    ;

    private Integer value;
    private String name;
    UserStatus(Integer value,String name){
        this.value = value;
        this.name = name;
    }
    /**
     * 枚举数据库存储值
     */
    @Override
    public Integer getValue() {
        return this.value;
    }
    public String getName() {
        return this.name;
    }
}
