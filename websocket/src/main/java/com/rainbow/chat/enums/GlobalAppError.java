package com.rainbow.chat.enums;

import com.alibaba.fastjson.annotation.JSONType;
import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.rainbow.chat.common.BaseEumDeserializer;
import com.rainbow.chat.common.BaseEnum;
import lombok.Getter;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

/**
 * ErrorCode
 *
 * @author ybtccc
 * @date 2018/4/3
 */
@Getter
@JSONType(deserializer = BaseEumDeserializer.class)
public enum GlobalAppError implements BaseEnum {
    OK(0,"操作成功")
    ,NEED_LOGIN(1,"需要登录")
    ,NEED_REALNAME(2,"需要实名")
    ,ERROR(9999,"系统错误")
    ;

    GlobalAppError(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    @JsonValue
    @EnumValue
    private Integer code;
    private String desc;
    private static final Map<Integer, GlobalAppError> lookup = new HashMap<>();
    static {
        for (GlobalAppError s : EnumSet.allOf(GlobalAppError.class)){
            lookup.put(s.getCode(), s);
        }
    }
    @Override
    public String toString() {
        return String.join(":", getCode()+"", getDesc());
    }
    @JsonCreator
    public static GlobalAppError fromValue(Integer value) {
        return lookup.get(value);
    }
}
