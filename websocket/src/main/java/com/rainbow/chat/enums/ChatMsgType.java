package com.rainbow.chat.enums;

import com.alibaba.fastjson.annotation.JSONType;
import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.rainbow.chat.common.BaseEnum;
import com.rainbow.chat.common.BaseEumDeserializer;
import lombok.Getter;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

@Getter
@JSONType(deserializer = BaseEumDeserializer.class)
public enum ChatMsgType implements BaseEnum {
    ERROR(0,"错误")
    ,TIP(1,"消息")
        , TIP_NEED_LOGIN(11,"消息-需要登陆")
        , TIP_NEED_INIT(11,"消息-需要初始化") //msg: sid
        , TIP_OFFLINE(12,"消息-离线")
        , TIP_ONLINE(13,"消息-在线")
        , TIP_INIT_OK(14,"消息-初始化成功")
        , TIP_PUSH_OK(15,"消息-推送成功") // msg: msgId
        , TIP_PUSH_FAIL(16,"消息-推送失败") // msg: msgId
        , TIP_PUSH_BACK(17,"消息-回撤") // msg: msgId
    ,ACTION(2,"动作")
        ,ACTION_INIT(21,"动作-初始化")
        ,ACTION_QUIT(22,"动作-下线")
        ,ACTION_GROUP_ADD(23,"动作-入群")
        ,ACTION_GROUP_QUIT(24,"动作-出群")
    ,CHAT(3,"聊天") //消息内容
        ,CHAT_GIFT(31,"聊天-收到礼物") // msg: 礼物id
        ,CHAT_FILE(32,"聊天-收到文件") // msg: 文件地址
    ,BOARDCAST(4,"广播") //msg: 广播内容
    ;

    @JsonValue
    @EnumValue
    private final Integer code;
    private String desc;
    ChatMsgType(Integer code, String desc){
        this.code = code;
        this.desc = desc;
    }

    private static final Map<Integer, ChatMsgType> lookup = new HashMap<>();
    static {
        for (ChatMsgType s : EnumSet.allOf(ChatMsgType.class)){
            lookup.put(s.getCode(), s);
        }
    }
    @Override
    public String toString() {
        return String.join(":", getCode()+"", getDesc());
    }
    @JsonCreator
    public static ChatMsgType fromValue(Integer code) {
        return lookup.get(code);
    }
}
