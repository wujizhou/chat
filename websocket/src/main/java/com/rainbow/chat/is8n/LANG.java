package com.rainbow.chat.is8n;

public class LANG {
    // symble
    public static final String SYMBLE_L  = "/";
    public static final String SYMBLE_R  = "\\";
    public static final String SYMBLE_B  = " ";
    public static final String SYMBLE_D  = ": ";
    public static final String SYMBLE_U  = "_";
    public static final String SYMBLE_M  = ", ";
    public static final String SYMBLE_LM  = "[ ";
    public static final String SYMBLE_RM  = " ]";
    // tip
    public static final String TIP_LOGIN_OK_DEVICE = "其他设备已登录,现已退出";
    public static final String TIP_LOGIN_FAIL_DEVICE  = "其他设备已登录,如需继续请重新登陆";
    //
    public static final String NEW_CONNECT  = "new connection";
    public static final String SUCCESS  = "成功";
    public static final String OK  = "成功";
    public static final String FAIL     = "失败";
    public static final String ERROR    = "失败";
    public static final String PING     = "ping";
    // tip
    public static final String INIT         = "初始化";
    public static final String USER_OFFLINE = "用户不在线";
    public static final String OUT_GROUP    = "退出群聊";
    public static final String IN_GROUP     = "进入群聊";
    public static final String NEED_INIT    = "需要初始化";
    public static final String NEED_LOGIN   = "需要登录";
    // error
    public static final String ERROR_NEED_IN_GROUP = "请先入群";
    public static final String ERROR_INVALID  = "非法操作";
    public static final String ERROR_GROUP    = "未知群组";
    public static final String ERROR_REFRESH  = "请刷新重试";
    public static final String ERROR_NOT_SUPPORT = "暂不支持";
    public static final String ERROR_LOGIN  = "登录失败";
    public static final String ERROR_FORMAT = "格式错误";


    public static String getLang(String key){
        return "TODO";
    }
}
