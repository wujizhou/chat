package com.rainbow.chat.jmx;

import com.rainbow.chat.common.ChatOnline;
import org.springframework.jmx.export.annotation.ManagedOperation;

import java.util.Map;

public interface IChatM {
    String getCount();

    @ManagedOperation(description = "统计")
    String count();

    @ManagedOperation(description = "在线列表")
    Map<Integer, ChatOnline> onlineMap();

    @ManagedOperation(description = "下线用户")
    void offline(Integer uid);
}
