package com.rainbow.chat.jmx;

import com.rainbow.chat.cache.WbRedisClientTemplate;
import com.rainbow.exception.BusinessException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jmx.export.annotation.ManagedOperation;
import org.springframework.jmx.export.annotation.ManagedResource;
import org.springframework.stereotype.Component;

@Component
@ManagedResource(
        objectName = "com.rainbow.chat.jmx:type=RedisM",
        description = "redis manager"
)
public class RedisM implements IRedisM {
    @Autowired
    private WbRedisClientTemplate<String,String> redis;
    /**
     * 查询redis
     * @param database
     * @param key
     * @return
     * @throws BusinessException
     */
    @ManagedOperation(description = "redis查看")
    @Override
    public String redisSelect(int database,String key) throws BusinessException {
        redis.select(database);
        return redis.get(key);
    }
}

