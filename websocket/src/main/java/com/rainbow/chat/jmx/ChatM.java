package com.rainbow.chat.jmx;

import com.rainbow.chat.common.ChatOnline;
import com.rainbow.chat.ws.WsServer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jmx.export.annotation.ManagedAttribute;
import org.springframework.jmx.export.annotation.ManagedOperation;
import org.springframework.jmx.export.annotation.ManagedResource;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
@ManagedResource(
        objectName = "com.rainbow.chat.jmx:type=ChatM",
        description = "chat manager"
)
public class ChatM implements IChatM {
    @Autowired
    WsServer wsServer;
    private String count;
    /**
     * 暴露属性
     */
    @ManagedAttribute(description = "获取统计属性")
    @Override
    public String getCount() {
        return count;
    }
    public void setCount(String count) {
        this.count = count;
    }


    /**
     * 暴露属性
     */
    @ManagedOperation(description = "统计")
    @Override
    public String count() {
        count = wsServer.getCount();
        return count;
    }

    /**
     * 暴露属性
     */
    @ManagedOperation(description = "在线列表")
    @Override
    public Map<Integer, ChatOnline> onlineMap() {
        return wsServer.queryOnline();
    }
    /**
     * 暴露属性
     */
    @ManagedOperation(description = "下线用户")
    @Override
    public void offline(Integer uid) {
        wsServer.offline(uid);
    }

    @Override
    public String toString() {
        return "Socket{" +
                "getCount='" + count + "'" +
                '}';
    }
}
