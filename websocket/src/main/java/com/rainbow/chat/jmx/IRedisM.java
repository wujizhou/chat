package com.rainbow.chat.jmx;

import com.rainbow.exception.BusinessException;

public interface IRedisM {
    String redisSelect(int database, String key) throws BusinessException;
}
